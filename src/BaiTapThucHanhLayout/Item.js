import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div className="col-lg-3 col-xxl-4 mb-5">
        <div className="card mx-2">
          <img src="./img/our-office-8.jpg" className="card-img-top" alt="" />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text pb-4">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
      </div>
    );
  }
}
